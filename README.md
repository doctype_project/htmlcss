# WalkBike # version 1.0 14/11/2016

## Краткая ифнормация ##

Любителям пеших и вело путешествий посвящается. Можно описать маршрут, даты, предполагаемое время путешествия. Любой может присоединиться.

### Ветка ilya. Справка ###

**Footer прижать к низу страницы**
```css
html,
body {
  height: 100%;
}
.wrapper {
  display: flex;
  height: 100%;
  flex-direction: column;
}
.flexbox {
  flex: 1 0 auto;
}
.footer {
  flex: 0 0 auto;
}
```
[Пример](http://dimox.name/examples/press-footer-bottom-with-css/example5.html)
[Другие варианты](http://dimox.name/press_footer_bottom_with_css)

**Картинки1-10 в lastwalk.html расположены с помощью Flexbox**

```css
.flex-container {
  display: flex;
  justify-content: space-between;
}
```

[Подробнее](http://html5.by/blog/flexbox/)